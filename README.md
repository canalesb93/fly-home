# Fly Home

Game Download: https://www.dropbox.com/s/qg0njut74zzo0k9/Walker-android.apk?dl=0

### Summary
A Libgdx framework mobile application. Game about an extraterrestrial being stuck in earth. He is trying to get home by floating upwards with a balloon. The player must avoid all obstacles and traverse up to space in the beautiful scenery the comes.

###### Notes:
Unfortunately code has been discontinued and is very dirty/unscalable.
You can however download the full working android .apk here:
- https://www.dropbox.com/s/qg0njut74zzo0k9/Walker-android.apk?dl=0

###### Install:
To install the .apk on your android device:
1. Permit external .apk's through your phones settings
2. Open mobile browser of your choice
3. Visit the following link: https://www.dropbox.com/s/qg0njut74zzo0k9/Walker-android.apk?dl=0
4. Follor installment procedures
5. Open the game through your applications menu