package com.me.walker;

import org.robovm.cocoatouch.foundation.NSAutoreleasePool;
import org.robovm.cocoatouch.uikit.UIApplication;

import com.badlogic.gdx.backends.iosrobovm.IOSApplication;
import com.badlogic.gdx.backends.iosrobovm.IOSApplicationConfiguration;

public class RobovmLauncher extends IOSApplication.Delegate {
	
		/* public class RobovmLauncher extends IOSApplication.Delegate */

	private static final String BANNER_AD_UNIT_ID = "BANNER SIZE iOS ID";
	private static final String LEADERBOARD_AD_UNIT_ID = "LEADERBOARD SIZE iOS ID";
	private static CGSize AD_SIZE;
	private static String AD_ID;
	private boolean adLoaded = false;
	private UIViewController rootViewController;
	private MPAdViewController adViewController;
	private MPAdView banner;
	
	
	/* protected IOSApplication createApplication() */
	
	
	@Override
	protected IOSApplication createApplication() {
		IOSApplicationConfiguration config = new IOSApplicationConfiguration();
		config.orientationLandscape = true;
		config.orientationPortrait = false;
		return new IOSApplication(new Walker(), config);
	}
	





	public boolean didFinishLaunching (UIApplication application, NSDictionary launchOptions) {
	      super.didFinishLaunching(application, launchOptions);
	  if(UIScreen.getMainScreen().getBounds().size().width() >= 728) {
	    System.out.println("Set as Leaderboard!");
	    AD_SIZE = MPConstants.MOPUB_LEADERBOARD_SIZE;
	    AD_ID = LEADERBOARD_AD_UNIT_ID;
	  } else {
	    System.out.println("Set as Banner!");
	    AD_SIZE = MPConstants.MOPUB_BANNER_SIZE;
	    AD_ID = BANNER_AD_UNIT_ID;
	  }

	  // We need a view controller to see ads.
	  //rootViewController = new UIViewController();
	  // If you are already using a UIWindow with a root view controller, get the root view controller (f.e. LibGDX):
	  rootViewController = application.getKeyWindow().getRootViewController();

	  // Create a MoPub ad. In this case a banner, but you can make it any size you want.
	  banner = new MPAdView(AD_ID, AD_SIZE);
	  // Let's calculate our banner size. We need to do this because the resolution of a retina and normal device is different.
	  float bannerWidth = UIScreen.getMainScreen().getBounds().size().width();
	  float bannerHeight = bannerWidth / AD_SIZE.width() * AD_SIZE.height();

	  // Let's set the frame (bounds) of our banner view. Remember on iOS view coordinates have their origin top-left.
	  // Position banner on the top.
	  banner.setFrame(new CGRect((UIScreen.getMainScreen().getBounds().size().width()/2)-AD_SIZE.width()/2, 0, bannerWidth, bannerHeight));
	  // Position banner on the bottom.
	  // banner.setFrame(new CGRect(0, UIScreen.getMainScreen().getBounds().size().height() - bannerHeight, bannerWidth, bannerHeight));
	  // Let's color the background for testing, so we can see if it is positioned correctly, even if no ad is loaded yet.
	  //banner.setBackgroundColor(new UIColor(1, 0, 0, 1)); // Remove this after testing.

	  // We use our custom ad view controller to notify for orientation changes.
	  adViewController = new MPAdViewController(banner);

	  // The delegate for the banner. It is required to override getViewController() to get ads.
	  MPAdViewDelegate bannerDelegate = new MPAdViewDelegate.Adapter() {
	    @Override
	    public UIViewController getViewController () {
	      return adViewController;
	    }
	  };
	  banner.setDelegate(bannerDelegate);
	  // Add banner to our view controller.
	  adViewController.getView().addSubview(banner);

	  // We add the ad view controller to our root view controller.
	  rootViewController.addChildViewController(adViewController);
	  rootViewController.getView().addSubview(adViewController.getView());

	  if(!adLoaded) {
	    banner.loadAd();
	    adLoaded = true;
	  }

	  // Create a standard UIWindow at screen size, add the view controller and show it.
	  application.getKeyWindow().setRootViewController(rootViewController);
	  application.getKeyWindow().addSubview(rootViewController.getView());
	  application.getKeyWindow().makeKeyAndVisible();
	  return false;
	}

	public static void main(String[] argv) {
		NSAutoreleasePool pool = new NSAutoreleasePool();
		UIApplication.main(argv, null, RobovmLauncher.class);
		pool.drain();
	}
}
