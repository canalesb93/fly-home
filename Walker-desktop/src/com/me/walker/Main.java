package com.me.walker;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Walker " + Walker.VERSION;
		cfg.width = 720;
		cfg.height = 1280;
		
		new LwjglApplication(new Walker(new ActionResolverDesktop(), new IActivityRequestHandler() {
			
			@Override
			public void showAds(boolean show) {
				Gdx.app.log("ADS", "showAds "+ show);
			}
		}), cfg);
	}
}
