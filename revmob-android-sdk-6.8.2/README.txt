RevMob Ads - Android SDK
-------------------------------

This package contains:

revmob-6.8.2.jar		- The SDK library 
apidocs/        		- Integration instructions and documentation (see index.html)
extras/					- Contains revmob-6.8.2-javadoc.jar (which you can attach to
						  the SDK library jar in Eclipse for contextual help)

The documentation (apidocs/) can also be found at http://sdk.revmob.com/sdks/android/docs/
						  
Best Regards,
The RevMob Team

