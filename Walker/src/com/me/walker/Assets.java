package com.me.walker;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class Assets {
	public static Texture background, stars, moon, floor, instructions;
	public static BitmapFont white, black;
	public static Sound pop;
	
	
	public static void load(){
		background=new Texture(Gdx.files.internal("images/cloudsAlpha.png"));
		instructions=new Texture(Gdx.files.internal("images/instructions.png"));
		stars=new Texture(Gdx.files.internal("images/stars.png"));
		moon=new Texture(Gdx.files.internal("images/moon.png"));
		floor=new Texture(Gdx.files.internal("images/floor.png"));
		pop = Gdx.audio.newSound(Gdx.files.internal("player/pop.mp3"));
		white = new BitmapFont(Gdx.files.internal("menu/visitor.fnt"));
		black = new BitmapFont(Gdx.files.internal("menu/visitorblack.fnt"));
	}
	
	public static void dispose(){
		background.dispose();
		stars.dispose();
		moon.dispose();
		floor.dispose();
		pop.dispose();
	}
	
}
