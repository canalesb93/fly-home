package com.me.walker;

import Screens.SplashScreens;

import com.badlogic.gdx.Game;

public class Walker extends Game {
	
	public static final String VERSION = "0.9.4 Beta";
	public static final String LOG = "Float Home";
	
	SplashScreens splashScreen;
	public static ActionResolver actionResolver;
	public static IActivityRequestHandler myRequestHandler;
	
	public Walker(ActionResolver actionResolver, IActivityRequestHandler handler) {
		Walker.actionResolver = actionResolver;
		Walker.myRequestHandler = handler;
//		Walker.ExternalHandler = irh;
	}

	@Override
	public void create() {
		Assets.load();
		setScreen(new SplashScreens(this));
		
		
	}

	@Override
	public void dispose() {
		Assets.dispose();
		super.dispose();
	}

	@Override
	public void render() {		
		super.render();

	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}
}
