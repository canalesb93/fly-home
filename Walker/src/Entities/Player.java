package Entities;

import Model.Data;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class Player {
	
	private Body body;
	private Fixture fixture;
	private Sprite playerSprite;
	public final float WIDTH=0.8f, HEIGHT=1;
	private boolean lose;
	

	public Player(World world, float x, float y) {
		
		lose = false;
		
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.position.set(x, y);
		bodyDef.fixedRotation =false;
		bodyDef.gravityScale = 5;
		
		//boxshape
		PolygonShape boxShape = new PolygonShape();
		boxShape.setAsBox(WIDTH, HEIGHT);
		
		//fixture definition
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = boxShape;
		fixtureDef.friction = 0.25f;
		fixtureDef.restitution = .1f;
		fixtureDef.density = 2.5f;
//		fixtureDef.filter.groupIndex = GROUP_PLAYER;
		
		body = world.createBody(bodyDef);
		fixture = body.createFixture(fixtureDef);
		
		//SPRITE
		playerSprite = new Sprite(new Texture("player/character2.1.png"));
		playerSprite.setSize(2.1f, 2.1f);
		playerSprite.setOrigin(playerSprite.getWidth()/2, playerSprite.getHeight()/2);
		//SPRITE
		  
		body.setUserData(new Data(this, playerSprite, 1));//ENTITY TYPE 1
		
		boxShape.dispose();
	}
	
	public void update(Vector2 movement){
		body.applyForceToCenter(movement, true);
	}

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public boolean isLose() {
		return lose;
	}

	public void setLose(boolean lose) {
		this.lose = lose;
	}

	public Fixture getFixture() {
		return fixture;
	}

	public void setFixture(Fixture fixture) {
		this.fixture = fixture;
	}

	public void dispose() {
		playerSprite.getTexture().dispose();
	}

}
