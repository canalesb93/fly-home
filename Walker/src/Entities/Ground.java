package Entities;


import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

public class Ground {
	
	private Body body, wall;
	private Fixture fixture;
	public Ground(World world, float ulx, float uly, float lx, float ly, float rx, float ry, float urx, float ury) {
		
		//---START GROUND-----------------------------------------------
		// body definition
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.StaticBody;
		bodyDef.position.set(0, 0);

		// side shapes
		ChainShape groundShape = new ChainShape();
		ChainShape wallShapes = new ChainShape();
		groundShape.createChain(new float[] {lx, ly, rx, ry,});
		
		// fixture definition
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = groundShape;
		fixtureDef.friction = .5f;
		fixtureDef.restitution = 0;

		body = world.createBody(bodyDef);
		fixture = body.createFixture(fixtureDef);
		
		wallShapes.createChain(new float[] {ulx, uly, lx,ly-1,rx,ry-1, urx, ury});
		
		// fixture definition
		fixtureDef.shape = wallShapes;
		fixtureDef.friction = 0f;
		fixtureDef.restitution = 0;

		wall = world.createBody(bodyDef);
		wall.createFixture(fixtureDef);
		
		wallShapes.dispose();
		
//		groundShape.createChain(new float[] {ulx, uly, lx, ly, rx, ry, urx, ury});
		
		//SPRITE
		/*
		playerSprite = new Sprite(new Texture("player/character2.png"));
		playerSprite.setSize(2.1f, 2.1f);
		playerSprite.setOrigin(playerSprite.getWidth()/2, playerSprite.getHeight()/2);
		*/
		//SPRITE
		  
		body.setUserData(this);//ENTITY TYPE 0
		
		groundShape.dispose();
		//---END GROUND-----------------------------------------------
		
	}
	
	public Body getBody() {
		return body;
	}
	public void setBody(Body body) {
		this.body = body;
	}
	
	public Body getWall() {
		return wall;
	}
	public void setWall(Body body) {
		this.wall = body;
	}
	public Fixture getFixture() {
		return fixture;
	}
	public void setFixture(Fixture fixture) {
		this.fixture = fixture;
	}

	
}
