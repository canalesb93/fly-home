package Entities;

import Model.Data;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.me.walker.Assets;

public class Floor {
	
	private Sprite floorSprite;
	private Body body, wall;
	private Fixture fixture;
	public Floor(World world, float ulx, float uly, float lx, float ly, float rx, float ry, float urx, float ury) {
		
		//---START GROUND-----------------------------------------------
		// body definition
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.StaticBody;
		bodyDef.position.set(0, 0);

		// side shapes
		ChainShape groundShape = new ChainShape();
		groundShape.createChain(new float[] {lx, 0, rx, 0,});
		
		// fixture definition
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = groundShape;
		fixtureDef.friction = 1f;
		fixtureDef.restitution = 0;

		body = world.createBody(bodyDef);
		fixture = body.createFixture(fixtureDef);

		
//		groundShape.createChain(new float[] {ulx, uly, lx, ly, rx, ry, urx, ury});
		
		//SPRITE
		
		floorSprite = new Sprite(Assets.floor);
		
		floorSprite.setOrigin(floorSprite.getWidth()/2, floorSprite.getHeight()/2);
		
		//SPRITE
		  
		
		
		groundShape.dispose();
		//---END GROUND-----------------------------------------------
		
	}
	
	public void setSpriteSize(float x, float y){
		floorSprite.setSize(x, y);
		floorSprite.setPosition(x, y);
		body.setUserData(new Data(this, floorSprite, 0));//ENTITY TYPE 0
	}
	
	public Body getBody() {
		return body;
	}
	public void setBody(Body body) {
		this.body = body;
	}
	
	public Body getWall() {
		return wall;
	}
	public void setWall(Body body) {
		this.wall = body;
	}
	public Fixture getFixture() {
		return fixture;
	}
	public void setFixture(Fixture fixture) {
		this.fixture = fixture;
	}

	public void dispose() {
	}
}
