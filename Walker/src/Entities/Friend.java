package Entities;

import Model.Data;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

public class Friend {
	
	private Body body;
	private Fixture fixture;
	private Sprite friendSprite;
	public float width, height;
	public Vector2 pos;
	private boolean remove;
	
	public Friend(World world, float x, float y, boolean remove) {
		
		this.remove = remove;
			
		/*----------------create BALLs----------------*/
		//body definition
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.position.set(0, 10);
		bodyDef.fixedRotation =false;
		bodyDef.gravityScale = 1;
		
		
		//ball shape
		CircleShape ballShape = new CircleShape();
		ballShape.setRadius(.5f);
		
		//fixture definiton
		FixtureDef fixtureDef = new FixtureDef(); 
		fixtureDef.shape = ballShape;
		fixtureDef.friction = .25f;		//friction
		fixtureDef.restitution = 0.75f;	//elasticity
//		fixtureDef.filter.groupIndex = GROUP_MONSTER;
		
		//spawnball
		
		
		ballShape.setPosition(new Vector2(0,0));
		//--------------END--FRIEND--END---------------------------		
		fixtureDef.density = 0.5f;		//weight
		bodyDef.gravityScale = 0;
		
		body = world.createBody(bodyDef);
		fixture = body.createFixture(fixtureDef);
		body.setActive(false);
		//--------------END--FRIEND--END---------------------------		
		ballShape.dispose();
			
		//SPRITE
		friendSprite = new Sprite(new Texture("player/friend1.png"));
		friendSprite.setSize(1.5f, 1.5f);
		friendSprite.setOrigin(friendSprite.getWidth()/2, friendSprite.getHeight()/2);
		
		body.setUserData(new Data(this, friendSprite, 2));//ENTITY TYPE 2
		//ENDSPRITE
		
	}

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public Fixture getFixture() {
		return fixture;
	}

	public void setFixture(Fixture fixture) {
		this.fixture = fixture;
	}

	public boolean isRemove() {
		return remove;
	}

	public void setRemove(boolean remove) {
		this.remove = remove;
		Gdx.app.log("REMOVE:", "queue for remove");
	}

	public Vector2 getPos() {
		return pos;
	}

	public void setPos(Vector2 pos) {
		this.pos = pos;
	}
	
	public void dispose() {
		friendSprite.getTexture().dispose();
	}

}
