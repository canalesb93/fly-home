package Model;

import com.badlogic.gdx.graphics.g2d.Sprite;

public class Data {
	
	private Object object;
	private Sprite sprite;
	private int entityType;
	
	public Data(Object object, Sprite sprite, int entityType) {
		super();
		this.object = object;
		this.sprite = sprite;
		this.entityType= entityType;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public Sprite getSprite() {
		return sprite;
	}

	public void setSprite(Sprite sprite) {
		this.sprite = sprite;
	}

	public int getEntityType() {
		return entityType;
	}

	public void setEntityType(int entityType) {
		this.entityType = entityType;
	}
	
	

}
