package Model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class LevelGenerator {

	private World world;
	private Body environment;
	private Sprite barSprite;
	private BodyDef bodyDef;
	private boolean scoreTrue;
	private Texture texture;
	
	private float leftEdge, rightEdge, minGap, maxGap, minWidth, maxWidth, height, angle, oldBar;

	public LevelGenerator(World world, float leftEdge, float rightEdge, float minGap, float maxGap, float minWidth, float maxWidth, float height, float angle) {
		this.world = world;
		this.leftEdge = leftEdge;
		this.rightEdge = rightEdge;
		this.minGap = minGap;
		this.maxGap = maxGap;
		this.minWidth = minWidth;
		this.maxWidth = maxWidth;
		this.height = height;
		this.angle = angle;
		scoreTrue = false;
		
		
		texture = new Texture("player/bar1.png");
	}
	
	public void generate(float topEdge) {
		float rand = MathUtils.random(minGap, maxGap);
		
		
		if(oldBar + rand > topEdge){
			return;
		}

		bodyDef = new BodyDef();
		bodyDef.type = BodyType.StaticBody;
		
		
		
		
		

		scoreTrue=true;
		oldBar = topEdge;//+0.5f;//TODO
		Gdx.app.log("oldBar", ""+oldBar);
		float width = MathUtils.random(minWidth, maxWidth);
		float x = MathUtils.random(leftEdge, rightEdge - width);
//		float rotation =MathUtils.random(-angle, angle);
		PolygonShape shape = new PolygonShape();
		bodyDef.position.set(new Vector2(x + width / 2, oldBar + height / 2));
		shape.setAsBox(width, height);//, new Vector2(0,0), rotation);		
		
		
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = shape;
		fixtureDef.friction = 0;
		fixtureDef.density = 0;
		Gdx.app.log("width", ""+width);
		environment = world.createBody(bodyDef);
		environment.createFixture(fixtureDef);
		
		//SPRITE
		
		barSprite = new Sprite(texture);
//		barSprite.setOrigin(barSprite.getWidth()/2, barSprite.getHeight()/2);
		barSprite.setSize(width*2, height*2);
//		barSprite.rotate(rotation);
		
		//SPRITE
		
		environment.setUserData(new Data(this, barSprite, 0));
		Gdx.app.log("POSITON", " "+environment.getPosition().x+" "+environment.getPosition().y);
		shape.dispose();
	}

	/*
	public Body getEnvironment() {
		return environment;
	}

	
	public void setEnvironment(Body environment) {
		this.environment = environment;
	}*/

	public boolean isScoreTrue() {
		return scoreTrue;
	}

	public void setScoreTrue(boolean scoreTrue) {
		this.scoreTrue = scoreTrue;
	}

	/** @return the {@link #leftEdge} */
	public float getLeftEdge() {
		return leftEdge;
	}

	/** @param leftEdge the {@link #leftEdge} to set */
	public void setLeftEdge(float leftEdge) {
		this.leftEdge = leftEdge;
	}

	/** @return the {@link #rightEdge} */
	public float getRightEdge() {
		return rightEdge;
	}

	/** @param rightEdge the {@link #rightEdge} to set */
	public void setRightEdge(float rightEdge) {
		this.rightEdge = rightEdge;
	}

	/** @return the {@link #minGap} */
	public float getMinGap() {
		return minGap;
	}

	/** @param minGap the {@link #minGap} to set */
	public void setMinGap(float minGap) {
		this.minGap = minGap;
	}

	/** @return the {@link #maxGap} */
	public float getMaxGap() {
		return maxGap;
	}

	/** @param maxGap the {@link #maxGap} to set */
	public void setMaxGap(float maxGap) {
		this.maxGap = maxGap;
	}

	/** @return the {@link #minWidth} */
	public float getMinWidth() {
		return minWidth;
	}

	/** @param minWidth the {@link #minWidth} to set */
	public void setMinWidth(float minWidth) {
		this.minWidth = minWidth;
	}

	/** @return the {@link #maxWidth} */
	public float getMaxWidth() {
		return maxWidth;
	}

	/** @param maxWidth the {@link #maxWidth} to set */
	public void setMaxWidth(float maxWidth) {
		this.maxWidth = maxWidth;
	}

	/** @return the {@link #height} */
	public float getHeight() {
		return height;
	}

	/** @param height the {@link #height} to set */
	public void setHeight(float height) {
		this.height = height;
	}

	public float getOldBar() {
		return oldBar;
	}

	public void setOldBar(float oldBar) {
		this.oldBar = oldBar;
	}
	
	public void dispose() {
		texture.dispose();
	}

}