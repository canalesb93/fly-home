package Model;

import Entities.Friend;
import Entities.Ground;
import Entities.Player;
import Screens.MainMenuReal;

import com.badlogic.gdx.Gdx;
//import com.badlogic.gdx.graphics.g2d.Sprite;
//import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.me.walker.Walker;

public class GameColision implements ContactListener{
	

	@Override
	public void beginContact(Contact contact) {
		// TODO Auto-generated method stub
	    Body a=contact.getFixtureA().getBody();
	    Body b=contact.getFixtureB().getBody();
	    
	    //-----------------FRIEND DIES---------------------
	    if(a.getUserData() instanceof Data&&b.getUserData() instanceof Data)
	    {
	    	if(((Data) b.getUserData()).getObject() instanceof LevelGenerator)
		    	if(((Data) a.getUserData()).getObject() instanceof Friend){
		    		Gdx.app.log("CONTACT", "SUCCESSWHATtheHELL");
		    		Friend friend = (Friend) ((Data) a.getUserData()).getObject();
		    		friend.setRemove(true);
		    	}
	    
	    	if(((Data) a.getUserData()).getObject() instanceof LevelGenerator)
		    	if(((Data) b.getUserData()).getObject() instanceof Friend){
		    		Gdx.app.log("CONTACT", "BALL SUCCESS");
		    		Friend friend = (Friend) ((Data) b.getUserData()).getObject();
		    		friend.setRemove(true);
		    	
		    	}
	    }
//	    -----------------PLAYER DIES---------------------
	    if(a.getUserData() instanceof Data&&b.getUserData() instanceof Ground)
	    {
	    	if(((Data) a.getUserData()).getObject() instanceof Player){
	    		Gdx.app.log("CONTACT", "playerSUCCESSWHATtheHELL");
	    		Player player = (Player) ((Data) b.getUserData()).getObject();
	    		player.setLose(true);
	    		((Walker) Gdx.app.getApplicationListener()).getScreen().dispose();
	    		((Walker) Gdx.app.getApplicationListener()).setScreen(new MainMenuReal());
	    	} 
	    	
	    } else if(b.getUserData() instanceof Data&&a.getUserData() instanceof Ground)
	    {
	    	if(((Data) b.getUserData()).getObject() instanceof Player){
	    		Gdx.app.log("CONTACT", "PLAYER SUCCESS");
	    		Player player = (Player) ((Data) b.getUserData()).getObject();
	    		player.setLose(true);

	    	
	    	}
	    }
		
	}

	@Override
	public void endContact(Contact contact) {
		
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		
	}

}
