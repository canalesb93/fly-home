package Screens;

import TweenAccessors.ActorAccessor;
import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.me.walker.Assets;
import com.me.walker.Walker;

public class GameOver implements Screen{

	private Stage stage;
	private TextureAtlas atlas;
	Walker game;

	private Skin skin;
	private Table table;
	private TextButton buttonPlay, buttonHighscore, buttonMenu;
	private Label labelScore;
	private Label hscore;
	private int highscore, score;
	private TweenManager tweenManager;
	private Preferences prefs = Gdx.app.getPreferences("myPreferences");
	
	private SpriteBatch batch;
	
	public GameOver(int score){
		this.score = score;
		
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		batch.begin();
		batch.draw(Assets.moon, Gdx.graphics.getWidth()/4 *3, Gdx.graphics.getHeight()/4 *3);
		batch.draw(Assets.stars, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		batch.end();
		
		tweenManager.update(delta);
		
		stage.act(delta);
		stage.draw();
//		Table.drawDebug(stage);//debug
	}

	@Override
	public void resize(int width, int height) {

		stage.getViewport().update(720, 1280, true);
		table.invalidateHierarchy();
		table.setSize(720, 1280);
	}

	@Override
	public void show() {
		Walker.myRequestHandler.showAds(true);
		
		stage = new Stage();
		
		Gdx.input.setInputProcessor(stage);
		Gdx.input.setCatchBackKey(false);//back button disable
		
		atlas = new TextureAtlas("menu/button2.pack");
		skin = new Skin(atlas);
		
		table = new Table(skin);
		table.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		
		//creating fonts
//		white = new BitmapFont(Gdx.files.internal("menu/whitefont.fnt"));
//		black = new BitmapFont(Gdx.files.internal("menu/blackfont.fnt"));
		
		batch = new SpriteBatch();
		
		//creating textbuttonstyle
		TextButtonStyle textButtonStyle = new TextButtonStyle();
		textButtonStyle.up = skin.getDrawable("button2");
		textButtonStyle.down = skin.getDrawable("button2pressed");
		textButtonStyle.pressedOffsetX = 1;
		textButtonStyle.pressedOffsetY = -1;
		textButtonStyle.font = Assets.black;
		textButtonStyle.font.setScale(1.5f);
		
		//creating playbutton
		buttonPlay = new TextButton("Play again!", textButtonStyle);
		buttonPlay.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.log("START", "Starting...again");
				GameOver.this.dispose();
				((Walker) Gdx.app.getApplicationListener()).setScreen(new GameScreen(false));
			}
		});
		buttonPlay.pad(20);
		
		//creating menuButton
		buttonMenu = new TextButton("Menu", textButtonStyle);
		
		buttonMenu.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.log("EXIT", "Exiting...to Menu");
				GameOver.this.dispose();
				((Walker) Gdx.app.getApplicationListener()).setScreen(new MainMenuReal());
			}
		});
		buttonMenu.pad(20);
				

		
		LabelStyle headingStyle = new LabelStyle(Assets.white, Color.WHITE);
		//LOADING FILE
		//loadfile TODO
		Boolean newScore = false;
		highscore = prefs.getInteger("highscore");
		if(score > highscore){
			//SAVE FILE
			//TODO
			prefs.putInteger("highscore", score);
			prefs.flush();
			Gdx.app.log("NEW HIGHSCORE", " "+score);
			highscore=score;
			hscore = new Label("NEW Highscore: "+Integer.toString(highscore), headingStyle);
			hscore.setFontScale(2.0f);
			
			
			newScore = true;
		} else {
			hscore = new Label("Highscore: "+Integer.toString(highscore), headingStyle);
			hscore.setFontScale(1.8f);
		}
		
		//creating heading

		labelScore = new Label("Score: "+Integer.toString(score), headingStyle);
		labelScore.setFontScale(2.2f);		
		
		//creating exitbutton
		buttonHighscore = new TextButton("Submit", textButtonStyle);
		
		buttonHighscore.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.log("submit: ", "highscore");
				if(Walker.actionResolver.getSignedInGPGS()==false){
					Walker.actionResolver.loginGPGS();
				}
				if (Walker.actionResolver.getSignedInGPGS()) {
					Walker.actionResolver.submitScoreGPGS(highscore);
					Walker.actionResolver.getLeaderboardGPGS();
				}
			}
		});
		buttonHighscore.pad(20);
		
		if (Walker.actionResolver.getSignedInGPGS()) {
			 if (highscore >= 5) Walker.actionResolver.unlockAchievementGPGS("CgkI_NDd5q4BEAIQAg");
			 if (highscore >= 15) Walker.actionResolver.unlockAchievementGPGS("CgkI_NDd5q4BEAIQAw");
			 if (highscore >= 45) Walker.actionResolver.unlockAchievementGPGS("CgkI_NDd5q4BEAIQBA");
			 if (highscore >= 80) Walker.actionResolver.unlockAchievementGPGS("CgkI_NDd5q4BEAIQBQ");
			 if (highscore >= 150) Walker.actionResolver.unlockAchievementGPGS("CgkI_NDd5q4BEAIQBg");
			}
		
		
		//creating sprite
//		texture = new Texture("player/character2.png");
//		Image walker = new Image(texture);
		
		//putting stuff together
//		table.add(walker).size(250);
//		table.getCell(walker).spaceBottom(50);
//		table.row();
		//imageEND
		
		
		table.add(labelScore).spaceBottom(50);
		table.row();
		table.add(hscore).spaceBottom(50);
		table.row();
		table.add(buttonPlay).size(400, 120);
		table.getCell(buttonPlay).spaceBottom(25);
		table.row();
		table.add(buttonHighscore).size(400, 100);
		table.getCell(buttonHighscore).spaceBottom(25);
		table.row();
		table.add(buttonMenu).size(400, 100);
		table.getCell(buttonMenu).spaceBottom(25);
		table.row();
		table.debug();//debug
		stage.addActor(table);
		
		//creating animations
		tweenManager = new TweenManager();
		Tween.registerAccessor(Actor.class , new ActorAccessor());
		
		//deploying animations
		if(newScore){
			Timeline.createSequence().beginSequence()
				.push(Tween.to(hscore, ActorAccessor.RGB, 0.25f).target(0,0,1))
				.push(Tween.to(hscore, ActorAccessor.RGB, 0.5f).target(0,1,0))
				.push(Tween.to(hscore, ActorAccessor.RGB, 0.5f).target(1,0,0))
				.push(Tween.to(hscore, ActorAccessor.RGB, 0.25f).target(0,0,1))
				.end().repeat(Tween.INFINITY, 0).start(tweenManager);
		}
		
		/*Timeline.createSequence().beginSequence()
			.push(Tween.set(buttonPlay, ActorAccessor.ALPHA).target(0))
			.push(Tween.set(buttonMenu, ActorAccessor.ALPHA).target(0))
			.push(Tween.from(labelScore, ActorAccessor.ALPHA, .5f).target(0))
			.push(Tween.to(buttonPlay, ActorAccessor.ALPHA, .5f).target(1))
			.push(Tween.to(buttonMenu, ActorAccessor.ALPHA, .5f).target(1))
			.end().start(tweenManager);*/
		
//		Tween.from(table, ActorAccessor.ALPHA, 0.5f).target(0).start(tweenManager);
//		Tween.from(table, ActorAccessor.ALPHA, 0.5f).target(Gdx.graphics.getHeight()/8).start(tweenManager);
		Tween.from(table, ActorAccessor.Y, .5f).target(Gdx.graphics.getHeight() / 8).start(tweenManager);

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		stage.dispose();
		atlas.dispose();
		skin.dispose();
//		white.dispose();
//		black.dispose();
		
		
	}
	
}
