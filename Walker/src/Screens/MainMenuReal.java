package Screens;

import TweenAccessors.ActorAccessor;
import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.me.walker.Assets;
import com.me.walker.Walker;

public class MainMenuReal implements Screen{

	private Stage stage;
	private TextureAtlas atlas;
	Walker game;

	private Skin skin;
	private Table table;
	private TextButton buttonPlay, buttonOptions, buttonHighscore, buttonAchievements;
	private Label heading;
	private Label hscore;
	private int highscore;
	private TweenManager tweenManager;
	private Texture texture;
	private Preferences prefs = Gdx.app.getPreferences("myPreferences");
	
	private SpriteBatch batch;
	
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		batch.draw(Assets.moon, Gdx.graphics.getWidth()/4 *3, Gdx.graphics.getHeight()/4 *3);
		batch.draw(Assets.background, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		batch.end();
		
		tweenManager.update(delta);
		
		stage.act(delta);
		stage.draw();
		Table.drawDebug(stage);//debug
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(720, 1280, true);
		table.invalidateHierarchy();
		table.setSize(720, 1280);
	}

	@Override
	public void show() {
		Walker.myRequestHandler.showAds(true);
		stage = new Stage();
		
		Gdx.input.setInputProcessor(stage);
		Gdx.input.setCatchBackKey(false);//back button disable
		
		atlas = new TextureAtlas("menu/button3.pack");
		skin = new Skin(atlas);
		
		table = new Table(skin);
		table.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		
		//creating fonts
		
//		black = new BitmapFont(Gdx.files.internal("menu/blackfont.fnt"));
		
		batch = new SpriteBatch();
		
		//creating textbuttonstyle
		TextButtonStyle textButtonStyle = new TextButtonStyle();
		textButtonStyle.up = skin.getDrawable("button3");
		textButtonStyle.down = skin.getDrawable("button3pressed");
		textButtonStyle.pressedOffsetX = 1;
		textButtonStyle.pressedOffsetY = -1;
		textButtonStyle.font = Assets.black;
		textButtonStyle.font.setScale(2f);
		
		//creating playbutton
		buttonPlay = new TextButton("Play", textButtonStyle);
		buttonPlay.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.log("START", "Starting...");
				MainMenuReal.this.dispose();
				((Walker) Gdx.app.getApplicationListener()).setScreen(new GameScreen(true));
			}
		});
		buttonPlay.pad(20);
		
		buttonOptions = new TextButton("Options", textButtonStyle);
		buttonOptions.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.log("OPTIONS", "Options...");
				Tween.to(table, ActorAccessor.Y, .75f).target(Gdx.graphics.getHeight() / 8).start(tweenManager);
				MainMenuReal.this.dispose();
				((Walker) Gdx.app.getApplicationListener()).setScreen(new Options());
			}
		});
		buttonOptions.pad(20);
		
		//creating exitbutton
		buttonHighscore = new TextButton("HIGHSCORE", textButtonStyle);
		
		buttonHighscore.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.log("login", "highscore...");
//				Gdx.app.exit();
				if (Walker.actionResolver.getSignedInGPGS()) Walker.actionResolver.getLeaderboardGPGS();
				else Walker.actionResolver.loginGPGS();
			}
		});
		buttonHighscore.pad(20);
		
		
		//creating buttonAchievements
		buttonAchievements = new TextButton("Trophies", textButtonStyle);
		
		buttonAchievements.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.log("login", "achievements...");
//				Gdx.app.exit();
				if (Walker.actionResolver.getSignedInGPGS()) Walker.actionResolver.getAchievementsGPGS();
				else Walker.actionResolver.loginGPGS();
			}
		});
		buttonAchievements.pad(20);
		//LOADING FILE
		highscore = prefs.getInteger("highscore");
		
		Gdx.app.log("FILE", "highscore"+highscore);
		
		
		
		//creating heading
		LabelStyle headingStyle = new LabelStyle(Assets.white, Color.WHITE);
		heading = new Label("Fly HOME", headingStyle);
		
		heading.setFontScale(3f);
		hscore = new Label("Highscore: "+Integer.toString(highscore), headingStyle);
		hscore.setFontScale(1.5f);
		
		//creating sprite
		texture = new Texture("player/character2.png");
		Image walker = new Image(texture);
		
		//putting stuff together
		table.add(walker).size(250);
		table.getCell(walker).spaceBottom(50);
		table.row();
		table.add(heading).spaceBottom(50);
		table.row();
		table.add(buttonPlay).size(400, 100);
		table.getCell(buttonPlay).spaceBottom(25);
		table.row();
		table.add(buttonOptions).size(400, 100);
		table.getCell(buttonOptions).spaceBottom(25);
		table.row();
		table.add(buttonHighscore).size(400, 100);
		table.getCell(buttonHighscore).spaceBottom(25);
		table.row();
		table.add(buttonAchievements).size(400, 100);
		table.getCell(buttonAchievements).spaceBottom(25);
		table.row();
		table.add(hscore).spaceBottom(50);
		table.debug();//debug
		stage.addActor(table);
		
		//creating animations
		tweenManager = new TweenManager();
		Tween.registerAccessor(Actor.class , new ActorAccessor());
		
		//deploying animations
//		Timeline.createSequence().beginSequence()
//			.push(Tween.to(heading, ActorAccessor.RGB, 0.5f).target(0,0,1))
//			.push(Tween.to(heading, ActorAccessor.RGB, 0.5f).target(0,1,0))
//			.push(Tween.to(heading, ActorAccessor.RGB, 0.5f).target(1,0,0))
//			.end().repeat(Tween.INFINITY, 0).start(tweenManager);
		
		Timeline.createSequence().beginSequence()
			.push(Tween.set(buttonPlay, ActorAccessor.ALPHA).target(0))
			.push(Tween.set(buttonOptions, ActorAccessor.ALPHA).target(0))
			.push(Tween.set(buttonHighscore, ActorAccessor.ALPHA).target(0))
			.push(Tween.set(buttonAchievements, ActorAccessor.ALPHA).target(0))
			.push(Tween.set(hscore, ActorAccessor.ALPHA).target(0))
			.beginParallel()
			.push(Tween.from(heading, ActorAccessor.ALPHA, 0.3f).target(0))
			//.pushPause(350)
			.push(Tween.to(buttonPlay, ActorAccessor.ALPHA, 0.6f).target(1))
			//.pushPause(350)
			.push(Tween.to(buttonOptions, ActorAccessor.ALPHA, 0.9f).target(1))
//			.pushPause(2000)
			.push(Tween.to(buttonHighscore, ActorAccessor.ALPHA, 1.2f).target(1))
			//.pushPause(350)
			.push(Tween.to(buttonAchievements, ActorAccessor.ALPHA, 1.5f).target(1))
			//.pushPause(350)
			.push(Tween.to(hscore, ActorAccessor.ALPHA, 1.8f).target(1))
			.end()
			.end().start(tweenManager);
		
//		Tween.from(table, ActorAccessor.ALPHA, 0.5f).target(0).start(tweenManager);
//		Tween.from(table, ActorAccessor.ALPHA, 0.5f).target(Gdx.graphics.getHeight()/8).start(tweenManager);
		Tween.from(table, ActorAccessor.Y, .75f).target(Gdx.graphics.getHeight() / 8).start(tweenManager);

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		stage.dispose();
		atlas.dispose();
		skin.dispose();
//		white.dispose();
//		black.dispose();
		texture.dispose();
//		background.dispose();
//		moon.dispose();
		
		
		
	}
	
}
