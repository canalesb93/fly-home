package Screens;

import rube.RubeScene;
import rube.loader.RubeSceneLoader;
import Entities.Floor;
import Entities.Friend;
import Entities.Ground;
import Entities.Player;
import Model.Data;
import Model.GameColision;
import Model.LevelGenerator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Input.Peripheral;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
//import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.joints.RopeJointDef;
import com.badlogic.gdx.utils.Array;
import com.me.walker.Assets;
import com.me.walker.Walker;

public class GameScreen implements Screen,GestureListener, InputProcessor{
	
//	private World world;
	private Box2DDebugRenderer debugRenderer;
	private SpriteBatch batch;
	private OrthographicCamera camera;
	
	private static final int TIMESTEP = 45;
//	private static final int VELOCITYITERATIONS = 8;
//	private static final int POSITIONITERATIONS = 3;
	private static final int MAPSCALE= 30;
	private static final float ALTITUDE= Gdx.graphics.getHeight()*0.35f;
	private static final float SPEED = 500;
	private static final float CLIMBSPEED = 40;
	
//	private static final float MAX_VELOCITY = 10;
	
	private LevelGenerator levelGenerator;
	private GameColision gameColision;
	
	private int score = -1, highscore;
	
	private boolean start=false, instruction=false;
	
	private Vector2 movement = new Vector2();
	private Player player;
	private Friend friend;
	private Ground ground;
	private Floor floor;
	
	
	//background colors
	private float red = 0
			 , green = 53
		     , blue = 91
		     , alpha = 0;
	
	
	private float posYBck1=0,posYBck2=0, moonpos=0,cameraOld=0, posYstars1=0,posYstars2=0;
	//Controls
	private boolean left, right;
	
	private Array<Body> tempBodies = new Array<Body>();
	
	private RubeSceneLoader loader;
	private RubeScene scene;
	
	private Vector3 botLeft, botRight, upLeft, upRight, midDown;
	
	private Preferences prefs = Gdx.app.getPreferences("myPreferences");
	private boolean accelerometer = false, instructions;
	private float accelX = Gdx.input.getAccelerometerX();
	
	public GameScreen(boolean a) {
		this.instructions = a;
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(red/255f, green/255f, blue/255f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		
//		world.step(TIMESTEP, VELOCITYITERATIONS, POSITIONITERATIONS);
		scene.step();
		
		if(accelerometer){
			accelX = Gdx.input.getAccelerometerX();
			accelX*=100;
			accelX*=2;
			if(accelX>600){
				accelX=600;
			}
			movement.x=-accelX;
//			Gdx.app.log("ACCELEROMETER", ""+accelX);
		}
		player.update(movement);
		friend.getBody().setLinearVelocity(0, CLIMBSPEED);
		if(friend.getBody().isActive()==false){
			friend.getBody().setTransform(player.getBody().getPosition(), 0);
		}
		
		if(friend.getBody().isActive()==true && friend.isRemove()){
			friend.getBody().setActive(false);
			if(prefs.getBoolean("sound")==false){
				Gdx.app.log("SOUNDS", "PLAY");
    			Assets.pop.play();
			}
    	
			//levelGenerator.getEnvironment().setActive(false);
//			scene.getWorld().destroyBody(friend.getBody());
//			scene.getWorld().destroyBody(levelGenerator.getEnvironment());
		}
		
		if(levelGenerator.isScoreTrue()){
			score+=1;
//			Gdx.app.log("SCORE", ""+score);
			levelGenerator.setScoreTrue(false);
			//TODO add sound
			if(red>0)
				red--;
			if(green>0)
				green-=0.5f;
			if(blue>0)
				blue--;
			if(score>50&&alpha<255){
				alpha+=2.5f;
			}
		}			
//		camera.position.y = player.getBody().getPosition().y > camera.position.y/2f ? player.getBody().getPosition().y : camera.position.y;
		if(player.getBody().getPosition().y +midDown.y> camera.position.y){
			camera.position.y = player.getBody().getPosition().y+midDown.y;
		}else if(start){
			camera.position.y += 1/5f;
		}
		
		camera.update();
		ground.getBody().setTransform(0, camera.position.y, 0);	
		ground.getWall().setTransform(0, camera.position.y, 0);
		
		
		
		debugRenderer.render(scene.getWorld(), camera.combined);
		
		
		batch.setProjectionMatrix(camera.combined);
		batch.begin();//BEGIN
		//BACKGROUND 
		posYBck1+=(camera.position.y-cameraOld)/2f;
		posYBck2+=(camera.position.y-cameraOld)/2f;
		moonpos+=(camera.position.y-cameraOld)/1.1f;
		posYstars1+=(camera.position.y-cameraOld)/1.2f;
		posYstars2+=(camera.position.y-cameraOld)/1.2f;
		
	    if(camera.position.y>=posYBck2+camera.viewportHeight/2){
	        posYBck1=posYBck2;
	    }
	    posYBck2=posYBck1+camera.viewportHeight;
	    
	    if(camera.position.y>=posYstars2+camera.viewportHeight/2){
	    	posYstars1=posYstars2;
	    }
	    posYstars2=posYstars1+camera.viewportHeight;
	    
	  
	    if(score>50){
	    	batch.setColor(1, 1, 1, alpha/255f);
	    	batch.draw(Assets.stars, -camera.viewportWidth/2, posYstars1, camera.viewportWidth, camera.viewportHeight);
		    batch.draw(Assets.stars, -camera.viewportWidth/2, posYstars2, camera.viewportWidth, camera.viewportHeight);
		    batch.setColor(1, 1, 1, 1);
	    }
	    batch.draw(Assets.moon, camera.viewportWidth/4, moonpos, 4,4);
	    batch.draw(Assets.background, -camera.viewportWidth/2, posYBck1, camera.viewportWidth, camera.viewportHeight);
	    batch.draw(Assets.background, -camera.viewportWidth/2, posYBck2, camera.viewportWidth, camera.viewportHeight);
	    //BACKGROUND
	    
		scene.getWorld().getBodies(tempBodies);
		
		for(Body body : tempBodies){
			if(body.getPosition().y < camera.position.y-50 ){
				scene.getWorld().destroyBody(body);
			}
			if(body.getUserData() != null && body.getUserData() instanceof Data){
				if(body == friend.getBody() && !friend.getBody().isActive()){
					
				}else{
				Sprite sprite = (Sprite) ((Data) body.getUserData()).getSprite();
				sprite.setPosition(body.getPosition().x-sprite.getWidth()/2, body.getPosition().y-sprite.getHeight()/2);
				sprite.setRotation(body.getAngle() * MathUtils.radiansToDegrees);
				sprite.draw(batch);
				}
			}
		}

		
//		white.draw(batch, "" + Gdx.graphics.getFramesPerSecond(), -camera.viewportWidth/4, camera.position.y + camera.viewportHeight/4);//FRAMES
		Assets.white.draw(batch, Integer.toString(score), 0-Assets.white.getBounds(Integer.toString(score)).width/2, camera.position.y - camera.viewportHeight/4);
		if(instruction==true){
			batch.draw(Assets.instructions, camera.position.x-camera.viewportWidth/2, camera.position.y-camera.viewportHeight/2, camera.viewportWidth, camera.viewportHeight);
		}
		
		batch.end();//END
		
		
		
		levelGenerator.generate(camera.position.y + camera.viewportHeight / 2);
		cameraOld=camera.position.y;
		
		if(player.isLose()){
			//if(Gdx.app.getType() == ApplicationType.Desktop)
			//	Gdx.graphics.setDisplayMode((int) (Gdx.graphics.getWidth()*2.5f), Gdx.graphics.getHeight(), false);
			GameScreen.this.dispose();
    		((Walker) Gdx.app.getApplicationListener()).setScreen(new GameOver(score));
		}
	}

	@Override
	public void resize(int width, int height) {
		width = 720;
		height = 1280;
		camera.viewportWidth = width/MAPSCALE;
		camera.viewportHeight= height/MAPSCALE;
		camera.update();

	}

	@Override
	public void show() {
		Walker.myRequestHandler.showAds(false);
		//SETUP
		//if(Gdx.app.getType() == ApplicationType.Desktop)
		//	Gdx.graphics.setDisplayMode((int) (Gdx.graphics.getWidth()*0.4f), Gdx.graphics.getHeight(), false);
		
		 
		
		Assets.white.setScale(0.2f);
		Assets.white.setUseIntegerPositions(false);
		
		
		Gdx.input.setInputProcessor(null);
//		world = new World(new Vector2(0, -9.81f), true);
		debugRenderer = new Box2DDebugRenderer();
		debugRenderer.setDrawBodies(false);
		debugRenderer.setDrawContacts(false);
		batch = new SpriteBatch();
		
		loader = new RubeSceneLoader();
		//LOAD JSON
	    scene = loader.loadScene(Gdx.files.internal("data/testSceneC.json"));
	    scene.stepsPerSecond=TIMESTEP;
	    
	    /*AssetManager mAssetManager = new AssetManager();
	    mAssetManager.setLoader(RubeScene.class, new RubeSceneAsyncLoader(new World(new Vector2(0,10),true),new InternalFileHandleResolver()));
	    mAssetManager.load(RUBE_SCENE_FILE, RubeScene.class);*/
	    
		camera = new OrthographicCamera(720/MAPSCALE, 1280/MAPSCALE);
		
		
		//CONTROLS
		InputMultiplexer im = new InputMultiplexer();
        GestureDetector gd = new GestureDetector(this);
        im.addProcessor(this);
        im.addProcessor(gd);		
		Gdx.input.setInputProcessor(im);
		Gdx.input.setCatchBackKey(true);//back button enable
		left=false;
		right=false;
		
//		Gdx.app.log("test", ""+Gdx.input.isPeripheralAvailable(Peripheral.Accelerometer));
		if(prefs.getBoolean("accelerometer")&&Gdx.input.isPeripheralAvailable(Peripheral.Accelerometer)){
			accelerometer=true;
		}
		
		
		posYBck1=posYBck2=-camera.viewportHeight/2;
		posYstars1=posYstars2=-camera.viewportHeight/2;
		if(instructions==true){
			instruction=true;
		}
		moonpos=camera.viewportHeight*1.5f;
//		Gdx.app.log("Load FILE", "current highscore is "+highscore);
		
		
//		scene.getWorld().setContactFilter(player);
		gameColision = new GameColision();
		scene.getWorld().setContactListener(gameColision);
		
		//ENTITY CREATION
		BodyDef bodyDef = new BodyDef();
//		FixtureDef fixtureDef = new FixtureDef();		
		
		//GRoundgen
		//---START WALLS----
				// body definition
				bodyDef.type = BodyType.StaticBody;
				bodyDef.position.set(0, 0);

				// side shapes
//				ChainShape wallShapes = new ChainShape();
				upLeft = new Vector3(0, 0, 0);
				upRight = new Vector3(Gdx.graphics.getWidth()-1, upLeft.y, 0);
				botLeft = new Vector3(0, Gdx.graphics.getHeight(), 0);
				botRight = new Vector3(Gdx.graphics.getWidth()-1, botLeft.y, 0);
				midDown = new Vector3(0, ALTITUDE, 0);
				
				camera.unproject(upLeft);
				camera.unproject(upRight);
				camera.unproject(botLeft);
				camera.unproject(botRight);
				camera.unproject(midDown);
				
		ground = new Ground(scene.getWorld(), upLeft.x, upLeft.y, botLeft.x, botLeft.y, botRight.x, botRight.y, upRight.x, upRight.y);
		floor = new Floor(scene.getWorld(), upLeft.x, upLeft.y, botLeft.x, botLeft.y, botRight.x, botRight.y, upRight.x, upRight.y);
		floor.setSpriteSize(camera.viewportWidth, camera.viewportHeight);
		
		player = new Player(scene.getWorld(), 0, 1);
		friend = new Friend(scene.getWorld(), 0, 1, false);
		
		
	
		//----------JOINT START-----------
		//RopeJOINT between player and friend----- 
		RopeJointDef ropeJointDef = new RopeJointDef();
		ropeJointDef.bodyA = player.getBody();
		ropeJointDef.bodyB = friend.getBody();
		ropeJointDef.maxLength = 3;
		ropeJointDef.collideConnected =true;
		ropeJointDef.localAnchorA.set(0, 1f);
		ropeJointDef.localAnchorB.set(0, -0.5f);
		
		scene.getWorld().createJoint(ropeJointDef);
		//---------JOINT END--------------
		
		
		//---START WALLS----
		// body definition
		bodyDef.type = BodyType.StaticBody;
		bodyDef.position.set(0, 0);

		// side shapes
//		ChainShape wallShapes = new ChainShape();
		upLeft = new Vector3(0, 0, 0);
		upRight = new Vector3(Gdx.graphics.getWidth()-1, upLeft.y, 0);
		botLeft = new Vector3(0, Gdx.graphics.getHeight(), 0);
		botRight = new Vector3(Gdx.graphics.getWidth()-1, botLeft.y, 0);
		midDown = new Vector3(0, ALTITUDE, 0);
		
		camera.unproject(upLeft);
		camera.unproject(upRight);
		camera.unproject(botLeft);
		camera.unproject(botRight);
		camera.unproject(midDown);
		

		
		
		
		///LEVELGENERATION
		levelGenerator = new LevelGenerator(scene.getWorld(),
				botLeft.x, botRight.x, //EDGE**dont touch
				player.HEIGHT*12, player.HEIGHT * 12, //DISTANCE HEIGHT
				player.WIDTH * 2f, player.WIDTH * 6f, //WIDTH
				player.WIDTH, 10 * MathUtils.degRad); // HEIGHT and ANGLE
		//TODO ADJUST
		
	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		scene.getWorld().dispose();
		scene.clear();
		debugRenderer.dispose();
		player.dispose();
		friend.dispose();
//		floor.dispose();
		levelGenerator.dispose();

	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		
		/*String message = "Zoom performed, initial Distance:" + Float.toString(initialDistance) +
                " Distance: " + Float.toString(distance);
        Gdx.app.log("INFO", message);
        float ratio = initialDistance/distance; 
		camera.zoom = ratio;*/
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2,
			Vector2 pointer1, Vector2 pointer2) {
		return false;
	}

	@Override
	public boolean keyDown(int keycode) {
		
		switch(keycode){
		case Keys.ESCAPE:
		case Keys.BACK:
			GameScreen.this.dispose();
			((Walker) Gdx.app.getApplicationListener()).setScreen(new MainMenuReal());
			break;
		case Keys.SPACE:
			if(friend.getBody().isActive()==false&&friend.isRemove()==false){
				instruction = false;
				start=true;
				friend.getBody().setActive(true);
				friend.getBody().setTransform(player.getBody().getPosition(), 0);
			}
			
			break;
		case Keys.W:	
			movement.y = SPEED;
			
			break;
		case Keys.A:	
			movement.x = -SPEED;
			break;
		case Keys.S:
			movement.y = -SPEED;
			break;
		case Keys.D:
			movement.x = SPEED;
			break;
		}
		if(left&&right){
			movement.x=0;
		}
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		switch(keycode){
		case Keys.ESCAPE:
		case Keys.BACK:
			GameScreen.this.dispose();
			((Walker) Gdx.app.getApplicationListener()).setScreen(new MainMenuReal());
			break;
			
		case Keys.W:
		case Keys.S:
			movement.y = 0;
			break;
		case Keys.A:	
		case Keys.D:
			movement.x = 0;
			break;
		}
		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		
		//int mitadY = Gdx.graphics.getHeight()/2;
		int mitadX = Gdx.graphics.getWidth()/2;
		System.out.println("X: "+screenX+"");
		System.out.println("Y: " +screenY);
		
		
		if(friend.getBody().isActive()==false&&friend.isRemove()==false){
			instruction = false;
			start=true;
			friend.getBody().setActive(true);
			friend.getBody().setTransform(player.getBody().getPosition(), 0);
		}
		if(accelerometer==false){
			if(screenX>mitadX){
				right=true;
				movement.x = SPEED;
				
			} else if(screenX<mitadX){
				left=true;
				movement.x = -SPEED;
			}
			if(left&&right){
				movement.x=0;
			}
		}
		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if(accelerometer==false){
			int mitadX = Gdx.graphics.getWidth()/2;
			if(screenX>mitadX){
				if(left){
					movement.x = -SPEED;
				}else{
					movement.x = 0;
				}
				right=false;
			} else if(screenX<mitadX){
				if(right){
					movement.x = SPEED;
				}else{
					movement.x = 0;
				}
				left=false;
			} 
		}
		
//		movement.set(0, 0);
		return true;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		camera.zoom += amount /15f;
//		Gdx.app.log("INFO", "zoom: "+camera.zoom);
		return true;
	}

}
