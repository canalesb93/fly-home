package Screens;

import TweenAccessors.SpriteTween;
import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.me.walker.Walker;

public class SplashScreens implements Screen{

	Texture splashTexture;
	Sprite splashSprite;
	SpriteBatch batch;
	Walker game;
	TweenManager manager;
	
	public SplashScreens(Walker walker) {
		this.game = walker;
		
	}

	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		manager.update(delta);
		
		if(Gdx.input.justTouched())
			((Walker) Gdx.app.getApplicationListener()).setScreen(new MainMenuReal());
//		if(Gdx.input.isKeyPressed(Input.Buttons.LEFT)){
//			game.setScreen(new MainMenu(game));
//			Gdx.app.log(Walker.LOG, "TESTT");
//		}
		
		batch.begin();
		splashSprite.draw(batch);
		batch.end();
	}

	@Override
	public void resize(int width, int height) {
		
		
	}

	@Override
	public void show() {
		Walker.myRequestHandler.showAds(false);
		splashTexture =new Texture("images/logoStart.png");
		splashTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		splashSprite = new Sprite(splashTexture);
		splashSprite.setColor(1,1,1,0);
//		splashSprite.setOrigin(splashSprite.getWidth()/2f, splashSprite.getHeight()/2f);
		splashSprite.setPosition(Gdx.graphics.getWidth()/2f-splashSprite.getWidth()/2, Gdx.graphics.getHeight()/2f-splashSprite.getHeight()/2);
		batch = new SpriteBatch();
		
		Tween.registerAccessor(Sprite.class, new SpriteTween());
		
		manager = new TweenManager();
		
		TweenCallback cb = new TweenCallback(){

			@Override
			public void onEvent(int type, BaseTween<?> source) {
				tweenCompleted();
				
			}
			
		};
		
		
		
		
		Tween.to(splashSprite, SpriteTween.ALPHA, 2f).target(1).ease(TweenEquations.easeInQuad).repeatYoyo(1, 1.5f).setCallback(cb).setCallbackTriggers(TweenCallback.COMPLETE).start(manager);
	}

	
	private void tweenCompleted(){
		Gdx.app.log(Walker.LOG, "SplashScreen Complete");
		((Walker) Gdx.app.getApplicationListener()).setScreen(new MainMenuReal());
	}
	
	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		
	}

}
