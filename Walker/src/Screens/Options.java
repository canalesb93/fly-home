package Screens;

import TweenAccessors.ActorAccessor;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.me.walker.Assets;
import com.me.walker.Walker;

public class Options implements Screen{

	private Stage stage;
	private TextureAtlas atlas;
	Walker game;

	private Skin skin;
	private Table table;
	private TextButton buttonSound, buttonReset, buttonBack, buttonAccelerometer;
	private BitmapFont black2;
	private Label heading;
//	private Label highscoreReset;
	private int highscore;
	private TweenManager tweenManager;
	private Preferences prefs = Gdx.app.getPreferences("myPreferences");
	
	private SpriteBatch batch;
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		batch.begin();
		batch.draw(Assets.moon, Gdx.graphics.getWidth()/4 *3, Gdx.graphics.getHeight()/4 *3);
		batch.draw(Assets.background, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		batch.end();
		
		tweenManager.update(delta);
		
		stage.act(delta);
		stage.draw();
//		Table.drawDebug(stage);//debug
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
		table.invalidateHierarchy();
		table.setSize(width, height);
	}

	@Override
	public void show() {
		Walker.myRequestHandler.showAds(true);
		stage = new Stage();
		
		Gdx.input.setInputProcessor(stage);
		Gdx.input.setCatchBackKey(false);//back button disable
		
		atlas = new TextureAtlas("menu/button2.pack");
		skin = new Skin(atlas);
		
		table = new Table(skin);
		table.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		
		//creating fonts
//		white = new BitmapFont(Gdx.files.internal("menu/whitefont.fnt"));
		black2 = new BitmapFont(Gdx.files.internal("menu/blackfont.fnt"));
		
		batch = new SpriteBatch();
		
		//creating textbuttonstyle
		TextButtonStyle textButtonStyle = new TextButtonStyle();
		textButtonStyle.up = skin.getDrawable("button2");
		textButtonStyle.down = skin.getDrawable("button2pressed");
		textButtonStyle.pressedOffsetX = 1;
		textButtonStyle.pressedOffsetY = -1;
		textButtonStyle.font = Assets.black;
		textButtonStyle.font.setScale(1.5f);
		
		//creating playbutton
		buttonSound = new TextButton("SOUND: ON", textButtonStyle);
		if(prefs.getBoolean("sound")==true){
			buttonSound.setText("SOUND: OFF");
		}
		
		buttonSound.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if(prefs.getBoolean("sound")==false){
					prefs.putBoolean("sound", true);
					buttonSound.setText("SOUND: OFF");
					prefs.flush();
				} else {
					prefs.putBoolean("sound", false);
					buttonSound.setText("SOUND: ON");
					prefs.flush();
				}
				
			}
		});
		buttonSound.pad(20);
		
		
		
		
		
		
		TextButtonStyle optionsButtonStyle = new TextButtonStyle();
		optionsButtonStyle.up = skin.getDrawable("button2");
		optionsButtonStyle.down = skin.getDrawable("button2pressed");
		optionsButtonStyle.pressedOffsetX = 1;
		optionsButtonStyle.pressedOffsetY = -1;
		optionsButtonStyle.font = black2;
//		optionsButtonStyle.font.setScale(1.5f);
		//creating reset button
		buttonReset = new TextButton("RESET HIGHSCORE: "+Integer.toString(prefs.getInteger("highscore")), optionsButtonStyle);
		
		buttonReset.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
					prefs.putInteger("highscore", 0);
					buttonReset.setText("RESET HIGHSCORE: 0");
					prefs.flush();
				
			}
		});
		buttonReset.pad(20);
		
		buttonAccelerometer = new TextButton("accelerometer: OFF", optionsButtonStyle);
		if(prefs.getBoolean("accelerometer")==true){
			buttonAccelerometer.setText("accelerometer: ON");
		}
		
		buttonAccelerometer.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if(prefs.getBoolean("accelerometer")==false){
					prefs.putBoolean("accelerometer", true);
					buttonAccelerometer.setText("accelerometer: ON");
					prefs.flush();
				} else {
					prefs.putBoolean("accelerometer", false);
					buttonAccelerometer.setText("accelerometer: OFF");
					prefs.flush();
				}
				
			}
		});
		buttonAccelerometer.pad(20);
		
		//creating exitbutton
		buttonBack = new TextButton("Back", textButtonStyle);
		
		buttonBack.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.log("BACK", "Exiting...to Menu");
				Options.this.dispose();
				((Walker) Gdx.app.getApplicationListener()).setScreen(new MainMenuReal());
			}
		});
		buttonBack.pad(20);
				
		
		LabelStyle headingStyle = new LabelStyle(Assets.white, Color.WHITE);
		//LOADING FILE
		//loadfile TODO
		highscore = prefs.getInteger("highscore");

		
		Gdx.app.log("FILE", "highscore"+highscore);
		
		//creating heading

		heading = new Label("Options", headingStyle);
		heading.setFontScale(2.2f);		
		

		
		//creating sprite
//		texture = new Texture("player/character2.png");
//		Image walker = new Image(texture);
		
		//putting stuff together
//		table.add(walker).size(250);
//		table.getCell(walker).spaceBottom(50);
//		table.row();
		//imageEND
		
		
		table.add(heading).spaceBottom(50);
		table.row();
		
		table.add(buttonSound).size(400, 100);
		table.getCell(buttonSound).spaceBottom(25);
		table.row();
		table.add(buttonAccelerometer).size(400, 100);
		table.getCell(buttonAccelerometer).spaceBottom(25);
		table.row();
		table.add(buttonReset).size(400, 100);
		table.getCell(buttonReset).spaceBottom(25);
		table.row();
		table.add(buttonBack).size(300, 100);
		table.getCell(buttonBack).spaceBottom(25);
		table.row();
		table.debug();//debug
		stage.addActor(table);
		
		//creating animations
		tweenManager = new TweenManager();
		Tween.registerAccessor(Actor.class , new ActorAccessor());
		
		//deploying animations
	
		/*Timeline.createSequence().beginSequence()
			.push(Tween.set(buttonPlay, ActorAccessor.ALPHA).target(0))
			.push(Tween.set(buttonMenu, ActorAccessor.ALPHA).target(0))
			.push(Tween.from(labelScore, ActorAccessor.ALPHA, .5f).target(0))
			.push(Tween.to(buttonPlay, ActorAccessor.ALPHA, .5f).target(1))
			.push(Tween.to(buttonMenu, ActorAccessor.ALPHA, .5f).target(1))
			.end().start(tweenManager);*/
		
//		Tween.from(table, ActorAccessor.ALPHA, 0.5f).target(0).start(tweenManager);
//		Tween.from(table, ActorAccessor.ALPHA, 0.5f).target(Gdx.graphics.getHeight()/8).start(tweenManager);
		Tween.from(table, ActorAccessor.Y, .5f).target(Gdx.graphics.getWidth()+100).start(tweenManager);

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		stage.dispose();
		atlas.dispose();
		skin.dispose();
//		white.dispose();
//		black.dispose();
		
		
	}
	
}
